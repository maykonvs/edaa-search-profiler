class LinkedListIterator:

    def __init__(self, instance):
        self._list = instance
        self._curr_node = instance.head

    def __next__(self):
        if self._curr_node is None:
            raise StopIteration
        
        value = self._curr_node.val
        self._curr_node = self._curr_node.next
        return value


class Node:

    def __init__(self):
        self.next = None
        self.prev = None
        self.val = -1  # Como trabalhamos apenas com valores positivos nos testes podemos considerar o -1 como um valor não setado.


class SearchList:
    is_sorted = False
    name = ""

    def append(self, value):
        raise NotImplementedError

    def copy(self):
        raise NotImplementedError

    def __len__(self):
        raise NotImplementedError

    def __iter__(self):
        raise NotImplementedError

    def __getitem__(self, key):
        raise NotImplementedError


class Vector(SearchList):
    name = "Vetor"

    def __init__(self, inner_list=None):
        self.__list = inner_list or list()

    def append(self, value):
        return self.__list.append(value)

    def copy(self):
        return Vector(inner_list=self.__list.copy())
    
    def __len__(self):
        return len(self.__list)

    def __iter__(self):
        return iter(self.__list)

    def __getitem__(self, key):
        return self.__list[key]

    def sort(self):
        self.__list.sort()


class LinkedList(SearchList):
    name = "Lista Encadeada"

    def __init__(self):
        self.head = None
        self.tail = None
        self.len = 0

        # Variáveis auxiliares para percorrer a lista com indexes.
        self.__curr_index = -1
        self.__curr_node = None
    
    def append(self, value):
        if self.head is None:
            # Lista vazia
            return self._add_empty(value)
        
        # Inserindo ao final
        return self._add_end(value)

    def _create_node(self, value):
        node = Node()
        node.val = value
        return node

    def _post_node_added(self):
        self.len += 1
    
    def _add_empty(self, value):
        node = self._create_node(value)

        self.head = node
        self.tail = node
        
        self._post_node_added()

        return node
    
    def _add_end(self, value):
        node = self._create_node(value)

        node.prev = self.tail
        self.tail.next = node
        self.tail = node
        
        self._post_node_added()

        return node

    def __len__(self):
        return self.len

    def __iter__(self):
        return LinkedListIterator(self)
    
    def __getitem__(self, key):
        if not isinstance(key, int):
            raise TypeError("O índice deve ser um integer!")

        if not self.__curr_node:
            self.__curr_node = self.head
            self.__curr_index = 0
        
        if not self.__curr_node or key >= self.len or key < -self.len:
            raise IndexError
        
        if key < 0:
            key += self.len
        
        while self.__curr_index < key:
            self.__curr_node = self.__curr_node.next
            self.__curr_index += 1
        
        while self.__curr_index > key:
            self.__curr_node = self.__curr_node.prev
            self.__curr_index -= 1
        
        return self.__curr_node.val

    
    def copy(self):
        ll = type(self)()

        ll._add_empty(self.head.val)

        node_it = self.head.next
        while node_it:
            ll._add_end(node_it.val)
            node_it = node_it.next
        
        return ll


class SortedLinkedList(LinkedList):
    is_sorted = True
    name = "Lista Encadeada Ordenada"

    def append(self, value):
        # Lista vazia    
        if self.head is None:
            return self._add_empty(value)

        # Otimização: Já verificando se o valor sendo adicionado não é maior que o valor do último elemento.
        # Neste caso o valor deve ser adicionado ao final.
        if value > self.tail.val:
            return self._add_end(value)

        # Procurando posição para inserir o elemento
        node_it = self.head
        while node_it and node_it.val < value:
            node_it = node_it.next
        
        if node_it == self.head:
            return self._add_start(value)

        return self._add_middle(node_it, value)

    def _add_start(self, value):
        node = self._create_node(value)

        self.head.prev = node
        node.next = self.head
        self.head = node
        
        self._post_node_added()

        return node
    
    def _add_middle(self, node_next, value):
        node = self._create_node(value)

        node_next.prev.next = node
        node.prev = node_next.prev
        node_next.prev = node
        node.next = node_next

        self._post_node_added()

        return node
