import unittest

import search
import structures as st


class TestSearchMethod(unittest.TestCase):

    def run_test_on(self, method_class):
        method = method_class()

        n = 20
        data = range(0, n, 2)  # Vetor ordenado para funcionar com todos os métodos de busca

        # Teste básico encontrando cada um dos elementos da lista
        for i, value in enumerate(data):
            pos = method.find(data, value)
            self.assertEqual(i, pos)
        
        # Testando retorno quando não encontra o elemento na lista
        pos = method.find(data, n)
        self.assertEqual(pos, -1)

    def test_sequential_search(self):
        self.run_test_on(search.SequentialSearchMethod)
    
    def test_sorted_sequential_search(self):
        self.run_test_on(search.SortedSequentialSearchMethod)
    
    def test_binary_search(self):
        self.run_test_on(search.BinarySearchMethod)
    
    def test_jump_search(self):
        self.run_test_on(search.JumpSearchMethod)


class SearchListImplementationTest(unittest.TestCase):
    search_list_class = None

    def setUp(self):
        if not self.search_list_class:
            raise unittest.SkipTest

    def _create_list_instance(self):
        return self.search_list_class()

    def test_name(self):
        self.assertNotEqual(self.search_list_class.name, "", "Defina um nome para a estrutura!")

    def test_append(self):
        l = self._create_list_instance()

        try:
            # Testa apenas se o append existe e não se ele funciona corretamente.
            l.append(1)
        except (AttributeError, NotImplementedError):
            self.fail("Método append() não implementado!")
        

    def test_len(self):
        l = self._create_list_instance()

        try:
            # Testa apenas se o len() funciona e não se ele funciona corretamente.
            len(l)
        except (TypeError, NotImplementedError):
            self.fail("Método __len__() não implementado!")

    def test_copy(self):
        ori = self._create_list_instance()
        
        ori.append(1)
        ori.append(2)
        ori.append(3)

        cp = ori.copy()

        self.assertIsInstance(cp, type(ori))  # A instância copiada deve ter o mesmo tipo que a original
        self.assertEqual(len(cp), len(ori))

        for i in range(0, len(ori)):
            self.assertEqual(cp[i], ori[i])
    
    def test_iter(self):
        l = self._create_list_instance()

        try:
            enumerate(l)
        except (TypeError, NotImplementedError):
            self.fail("Método __iter__() não implementado!")

    def test_getitem(self):
        l = self._create_list_instance()

        l.append(0)

        try:
            l[0]
        except (TypeError, NotImplementedError):
            self.fail("Método __getitem__() não implementado!")


# Criando dinamicamente TestCase's para as implementações de listas.
def create_search_list_implementation_test_cases():
    glbs = globals()  # Pegando o dicionário de atributos deste módulo

    # Pegando as classes que estendem a SearchList
    classes = dict([(name, c) for name, c in st.__dict__.items() if isinstance(c, type) and c != st.SearchList and issubclass(c, st.SearchList)])

    for c in classes:
        # Criando um TestCase
        name = "Test" + c + "Implementation"
        test_case = type(name, (SearchListImplementationTest,), {})
        test_case.search_list_class = classes[c]
        glbs[name] = test_case


def list_copy_test(test_case, list_class):
    ll_1 = list_class()

    for n in range(0, 10):
        ll_1.append(n)

    ll_2 = ll_1.copy()

    test_case.assertIsInstance(ll_2, list_class)
    test_case.assertEqual(len(ll_1), len(ll_2))

    def assert_node(node_1, node_2):
        test_case.assertNotEqual(node_1, node_2)
        test_case.assertEqual(node_1.val, node_2.val)

    assert_node(ll_1.head, ll_2.head)
    assert_node(ll_1.tail, ll_2.tail)

    node_it_1 = ll_1.head.next
    node_it_2 = ll_2.head.next
    
    while node_it_1 and node_it_2:
        assert_node(node_it_1, node_it_2)
        assert_node(node_it_1.prev, node_it_2.prev)

        node_it_1 = node_it_1.next
        node_it_2 = node_it_2.next


def list_getitem_test(test_case, list_class):
    ll = list_class()

    ll.append(0)
    ll.append(1)
    ll.append(2)

    test_case.assertEqual(ll[0], 0)
    test_case.assertEqual(ll[1], 1)
    test_case.assertEqual(ll[2], 2)
    test_case.assertEqual(ll[-1], 2)
    test_case.assertEqual(ll[-2], 1)
    test_case.assertEqual(ll[-3], 0)
    test_case.assertEqual(ll[2], 2)


class TestLinkedList(unittest.TestCase):

    def test_empty_list(self):
        ll = st.LinkedList()

        self.assertIs(ll.head, None)
        self.assertIs(ll.tail, None)
        self.assertEqual(len(ll), 0)
    
    def test_one_item_list(self):
        ll = st.LinkedList()

        node = ll.append(0)

        self.assertEqual(ll.head, node)
        self.assertEqual(ll.tail, node)
        self.assertEqual(len(ll), 1)
        self.assertEqual(node.val, 0)
        self.assertIs(node.prev, None)
        self.assertIs(node.next, None)
    
    def test_many_items_list(self):
        ll = st.LinkedList()

        node_0 = ll.append(0)
        node_1 = ll.append(1)
        node_2 = ll.append(2)

        self.assertEqual(ll.head, node_0)
        self.assertEqual(ll.tail, node_2)
        self.assertEqual(len(ll), 3)
        self.assertEqual(node_0.val, 0)
        self.assertIs(node_0.prev, None)
        self.assertEqual(node_0.next, node_1)
        self.assertEqual(node_1.val, 1)
        self.assertEqual(node_1.prev, node_0)
        self.assertEqual(node_1.next, node_2)
        self.assertEqual(node_2.val, 2)
        self.assertEqual(node_2.prev, node_1)
        self.assertIs(node_2.next, None)

    def test_copy(self):
        list_copy_test(self, st.LinkedList)

    def test_iteration(self):
        ll = st.LinkedList()

        for n in range(0, 10):
            ll.append(n)
        
        for i, n in enumerate(ll):
            self.assertEqual(i, n)

    def test_getitem(self):
        list_getitem_test(self, st.LinkedList)


class TestSortedLinkedList(unittest.TestCase):

    def test_empty_list(self):
        ll = st.SortedLinkedList()

        self.assertIs(ll.head, None)
        self.assertIs(ll.tail, None)
        self.assertEqual(len(ll), 0)
    
    def test_one_item_list(self):
        ll = st.SortedLinkedList()

        node = ll.append(0)

        self.assertEqual(ll.head, node)
        self.assertEqual(ll.tail, node)
        self.assertEqual(len(ll), 1)
        self.assertEqual(node.val, 0)
        self.assertIs(node.prev, None)
        self.assertIs(node.next, None)
    
    def test_many_items_list(self):
        ll = st.SortedLinkedList()

        node_2 = ll.append(2)
        node_0 = ll.append(0)
        node_1 = ll.append(1)
        node_3 = ll.append(3)

        self.assertEqual(ll.head, node_0)
        self.assertEqual(ll.tail, node_3)
        self.assertEqual(len(ll), 4)
        self.assertEqual(node_0.val, 0)
        self.assertIs(node_0.prev, None)
        self.assertEqual(node_0.next, node_1)
        self.assertEqual(node_1.val, 1)
        self.assertEqual(node_1.prev, node_0)
        self.assertEqual(node_1.next, node_2)
        self.assertEqual(node_2.val, 2)
        self.assertEqual(node_2.prev, node_1)
        self.assertEqual(node_2.next, node_3)
        self.assertEqual(node_3.val, 3)
        self.assertEqual(node_3.prev, node_2)
        self.assertIs(node_3.next, None)

    def test_copy(self):
        list_copy_test(self, st.SortedLinkedList)
    
    def test_getitem(self):
        list_getitem_test(self, st.SortedLinkedList)


if __name__ == '__main__':
    create_search_list_implementation_test_cases()
    unittest.main()
