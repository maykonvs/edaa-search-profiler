from time import perf_counter_ns


class ProfileResult:
    '''
    Resultado de um teste.
    '''

    '''
    Possíveis categorias de resultados da busca.
    '''
    RESULT_CATEGORY_NONE = 'NN'  # Nenhuma categoria (apenas valor inicial)
    RESULT_CATEGORY_NOT_FOUND = 'NF'  # Valor não encontrado
    RESULT_CATEGORY_FIRST_10 = 'FI'  # Valor encontrado nas 10 primeiras posições
    RESULT_CATEGORY_LAST_10 = 'LA'  # Valor encontrado nas 10 últimas posições
    RESULT_CATEGORY_CENTER = 'CE'  # Valor encontrado no centro (qualquer posição que não seja as 10 primeiras ou as 10 últimas)

    def __init__(self):
        self.search_method = ""  # Nome do método de busca
        self.data_structure = ""  # Nome da estrutura que contém os dados
        self.search_value = -1  # O valor buscado
        self.search_result = -1  # A posição encontrada (-1 == não encontrado)
        self.data_size = 0  # tamanho do vetor de entrada
        self.sort_execution_time = 0  # Tempo de execução da ordenação (se necessário) (ns)
        self.search_execution_time = 0  # Tempo de execução da busca (ns)
        self.total_execution_time = 0  # Tempo total de execução (ordenação + busca) (ns)
        self.result_category = ProfileResult.RESULT_CATEGORY_NONE  # Categoria do resultado da busca
    
    def set_result(self, search_method, data_structure, search_value, search_result, data_size, sort_execution_time, total_execution_time):
        self.search_method = search_method
        self.data_structure = data_structure
        self.search_value = search_value
        self.search_result = search_result
        self.data_size = data_size
        self.sort_execution_time = sort_execution_time
        self.search_execution_time = total_execution_time - sort_execution_time
        self.total_execution_time = total_execution_time

        if search_result < 0:
            self.result_category = ProfileResult.RESULT_CATEGORY_NOT_FOUND
        elif search_result < 10:
            self.result_category = ProfileResult.RESULT_CATEGORY_FIRST_10
        elif search_result > data_size - 10:
            self.result_category = ProfileResult.RESULT_CATEGORY_LAST_10
        else:
            self.result_category = ProfileResult.RESULT_CATEGORY_CENTER

class ProfileTest:
    '''
    Teste de performance.
    '''
    def __init__(self):
        self.method = None  # Método de busca
        self.data = None  # Vetor de dados
        self.value = -1  # Valor sendo buscado

    def test(self):
        '''
        Função principal que realiza o teste de performance.
        '''
        start = perf_counter_ns()

        if self.method.requires_sort:
            if not self.data.is_sorted:
                self.data.sort()
            end_sort = perf_counter_ns()
        else:
            end_sort = start

        position = self.method.find(self.data, self.value)

        end = perf_counter_ns()

        result = ProfileResult()
        result.set_result(self.method.name, type(self.data).name, self.value, position, len(self.data), end_sort - start, end - start)

        return result
