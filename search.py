import math


class SearchMethod:
    '''
    Classe base para métodos de busca.
    '''
    def __init__(self):
        self.name = ""
        self.requires_sort = False
    
    def find(self, data, value):
        '''
        Função que busca um valor em um vetor. Caso o valor não seja encontrado deve retornar -1.
        '''
        pass


class SequentialSearchMethod(SearchMethod):
    '''
    Busca sequencial padrão.
    '''
    def __init__(self):
        super().__init__()
        self.name = "Busca Sequencial"

    def find(self, data, value):
        for i, v in enumerate(data):
            if v == value:
                return i
        
        return -1


class SortedSequentialSearchMethod(SearchMethod):
    '''
    Busca sequencial otimizada para valores sequenciais.
    '''
    def __init__(self):
        super().__init__()
        self.name = "Busca Sequencial Ordenada"
        self.requires_sort = True

    def find(self, data, value):
        for i, v in enumerate(data):
            if v == value:
                return i
            if v > value:
                return -1
        
        return -1


class BinarySearchMethod(SearchMethod):
    '''
    Busca binária.
    '''
    def __init__(self):
        self.name = "Busca Binária"
        self.requires_sort = True
    
    def find(self, data, value):
        start = 0
        end = len(data) - 1

        while start <= end:
            middle = int((start + end) / 2)
            
            if data[middle] == value:
                return middle
            
            if data[middle] < value:
                start = middle + 1
            else:
                end = middle - 1
        
        return -1


class JumpSearchMethod(SearchMethod):
    '''
    Busca em saltos.
    '''
    def __init__(self):
        self.name = "Busca em Saltos"
        self.requires_sort = True
    
    def find(self, data, value):
        if data[0] > value:
            return -1

        n = len(data)
        step = int(math.sqrt(n))

        i = step
        j = 0

        while i < n and data[i] <= value:
            j = i
            i += step
        
        if i >= n:
            i = n
        
        for k in range(j, i):
            if data[k] == value:
                return k
        
        return -1
