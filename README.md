Este projeto foi feito e testado utilizado o python versão 3.9.2.

Após clonar o projeto é possível fazer os testes para gerar o arquivo de resultados.

Para isto primeiro é necessário gerar os vetores de entrada:

```shell
python inputs_gen.py
```

Após isto serão gerados vários arquivos de entradas (v10.input, v11.input, ..., v20.input).

Com isto basta rodar o script que roda os testes:

```shell
python run_profile.py
```

Ao final dos testes os resultados estão disponíveis no arquivo output.csv, permitindo a extração de estatísticas utilizando qualquer outra ferramenta disponível.

Após modificações nas implementações nos métodos de busca é possível rodar o script de testes unitários para verificar se as implementações estão corretas:

```shell
python test.py
```
