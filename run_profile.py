import random
import glob
import os
from time import perf_counter_ns

import profile
import search
import serializers
import structures


random.seed(10)


class RunnerResult:

    def __init__(self):
        self.search_method = ""  # Nome do método de busca
        self.data_structure = ""  # Nome da estrutura que contém os dados
        self.data_size = 0  # tamanho do vetor de entrada
        self.data_gen_time = 0  # Tempo de execução do preenchimento da estrutura de dados de entrada
        self.total_execution_time = 0  # Tempo total de execução (todas as buscas)
    
    def set_result(self, search_method, data_structure, data_size, data_gen_time, total_execution_time):
        self.search_method = search_method
        self.data_structure = data_structure
        self.data_size = data_size
        self.data_gen_time = data_gen_time
        self.total_execution_time = total_execution_time


class Runner:

    def _get_data(self, input, data_class):
        file = open(input, "r")

        data = data_class()

        # Preenche a estrutura de entrada.
        # Verificando tempo que isto leva.
        start = perf_counter_ns()
        for line in file.readlines():
            data.append(int(line))
        end = perf_counter_ns()
        
        file.close()

        return data, end - start

    def run(self, input, srch, data_class, method_class):
        start = perf_counter_ns()

        method = method_class()

        data, data_gen_time = self._get_data(input, data_class)
        values, _ = self._get_data(srch, list)

        n = len(data)

        profile_test = profile.ProfileTest()
        profile_test.method = method

        results = list()

        for v in values:
            profile_test.data = data.copy() if not data_class.is_sorted and method.requires_sort else data
            profile_test.value = v
            results.append(profile_test.test())

        end = perf_counter_ns()

        result = RunnerResult()
        result.set_result(method.name, data_class.name, n, data_gen_time, end-start)

        return result, results


if __name__ == '__main__':
    runner = Runner()

    test_cases = (
        ('v10.input', 'v10.search', structures.Vector, search.SequentialSearchMethod),
        ('v11.input', 'v11.search', structures.Vector, search.SequentialSearchMethod),
        ('v12.input', 'v12.search', structures.Vector, search.SequentialSearchMethod),
        ('v13.input', 'v13.search', structures.Vector, search.SequentialSearchMethod),
        ('v14.input', 'v14.search', structures.Vector, search.SequentialSearchMethod),
        ('v15.input', 'v15.search', structures.Vector, search.SequentialSearchMethod),
        ('v16.input', 'v16.search', structures.Vector, search.SequentialSearchMethod),
        ('v17.input', 'v17.search', structures.Vector, search.SequentialSearchMethod),
        ('v18.input', 'v18.search', structures.Vector, search.SequentialSearchMethod),
        ('v19.input', 'v19.search', structures.Vector, search.SequentialSearchMethod),
        ('v20.input', 'v20.search', structures.Vector, search.SequentialSearchMethod),
        ('v10.input', 'v10.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v11.input', 'v11.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v12.input', 'v12.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v13.input', 'v13.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v14.input', 'v14.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v15.input', 'v15.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v16.input', 'v16.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v17.input', 'v17.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v18.input', 'v18.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v19.input', 'v19.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v20.input', 'v20.search', structures.LinkedList, search.SequentialSearchMethod),
        ('v10.input', 'v10.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v11.input', 'v11.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v12.input', 'v12.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v13.input', 'v13.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v14.input', 'v14.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v15.input', 'v15.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v16.input', 'v16.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v17.input', 'v17.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v18.input', 'v18.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v19.input', 'v19.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v20.input', 'v20.search', structures.Vector, search.SortedSequentialSearchMethod),
        ('v10.input', 'v10.search', structures.SortedLinkedList, search.SortedSequentialSearchMethod),
        ('v11.input', 'v11.search', structures.SortedLinkedList, search.SortedSequentialSearchMethod),
        ('v12.input', 'v12.search', structures.SortedLinkedList, search.SortedSequentialSearchMethod),
        ('v13.input', 'v13.search', structures.SortedLinkedList, search.SortedSequentialSearchMethod),
        ('v14.input', 'v14.search', structures.SortedLinkedList, search.SortedSequentialSearchMethod),
        ('v15.input', 'v15.search', structures.SortedLinkedList, search.SortedSequentialSearchMethod),
        ('v10.input', 'v10.search', structures.Vector, search.BinarySearchMethod),
        ('v11.input', 'v11.search', structures.Vector, search.BinarySearchMethod),
        ('v12.input', 'v12.search', structures.Vector, search.BinarySearchMethod),
        ('v13.input', 'v13.search', structures.Vector, search.BinarySearchMethod),
        ('v14.input', 'v14.search', structures.Vector, search.BinarySearchMethod),
        ('v15.input', 'v15.search', structures.Vector, search.BinarySearchMethod),
        ('v16.input', 'v16.search', structures.Vector, search.BinarySearchMethod),
        ('v17.input', 'v17.search', structures.Vector, search.BinarySearchMethod),
        ('v18.input', 'v18.search', structures.Vector, search.BinarySearchMethod),
        ('v19.input', 'v19.search', structures.Vector, search.BinarySearchMethod),
        ('v20.input', 'v20.search', structures.Vector, search.BinarySearchMethod),
        ('v10.input', 'v10.search', structures.SortedLinkedList, search.BinarySearchMethod),
        ('v11.input', 'v11.search', structures.SortedLinkedList, search.BinarySearchMethod),
        ('v12.input', 'v12.search', structures.SortedLinkedList, search.BinarySearchMethod),
        ('v13.input', 'v13.search', structures.SortedLinkedList, search.BinarySearchMethod),
        ('v14.input', 'v14.search', structures.SortedLinkedList, search.BinarySearchMethod),
        ('v15.input', 'v15.search', structures.SortedLinkedList, search.BinarySearchMethod),
        ('v10.input', 'v10.search', structures.Vector, search.JumpSearchMethod),
        ('v11.input', 'v11.search', structures.Vector, search.JumpSearchMethod),
        ('v12.input', 'v12.search', structures.Vector, search.JumpSearchMethod),
        ('v13.input', 'v13.search', structures.Vector, search.JumpSearchMethod),
        ('v14.input', 'v14.search', structures.Vector, search.JumpSearchMethod),
        ('v15.input', 'v15.search', structures.Vector, search.JumpSearchMethod),
        ('v16.input', 'v16.search', structures.Vector, search.JumpSearchMethod),
        ('v17.input', 'v17.search', structures.Vector, search.JumpSearchMethod),
        ('v18.input', 'v18.search', structures.Vector, search.JumpSearchMethod),
        ('v19.input', 'v19.search', structures.Vector, search.JumpSearchMethod),
        ('v20.input', 'v20.search', structures.Vector, search.JumpSearchMethod),
        ('v10.input', 'v10.search', structures.SortedLinkedList, search.JumpSearchMethod),
        ('v11.input', 'v11.search', structures.SortedLinkedList, search.JumpSearchMethod),
        ('v12.input', 'v12.search', structures.SortedLinkedList, search.JumpSearchMethod),
        ('v13.input', 'v13.search', structures.SortedLinkedList, search.JumpSearchMethod),
        ('v14.input', 'v14.search', structures.SortedLinkedList, search.JumpSearchMethod),
        ('v15.input', 'v15.search', structures.SortedLinkedList, search.JumpSearchMethod),
    )

    runner_results = list()
    search_results = list()

    for case in test_cases:
        runner_result, search_result = runner.run(*case)
        runner_results.append(runner_result)
        search_results += search_result
    
    serializer = serializers.CSVSerializer()
    serializer.serialize(runner_results, "output_runner.csv")
    serializer.serialize(search_results, "output_search.csv")
