import random

random.seed(20)

def create_file(n):
    filename = "v{size}.input".format(size=n)
    file = open(filename, "w")

    input_len = pow(2, n)
    count = int(input_len * 1.25)  # Vetor 1.25x maior que o número de elementos que queremos.
    v = list(range(0, count))
    random.shuffle(v)

    # Gravando apenas os n elementos que queremos, assim ficamos com um vetor com algumas lacunas entre os elementos.
    for i in v[0:input_len]:
        file.write("{0}\n".format(i))

    file.close()

    filename = "v{size}.search".format(size=n)
    file = open(filename, "w")

    a = v[:10]  # Forçando os 10 primeiros elementos da lista para termos resultados na categoria FI
    a += v[input_len-10:input_len]  # Forçando os 10 últimos elementos da lista para termos resultados na categoria LA

    # Gerando os elementos restantes.
    # Jogan
    for _ in range(80):
        a.append(random.randint(0, count))

    random.shuffle(a)
    
    # Gravando apenas os n elementos que queremos, assim ficamos com um vetor com algumas lacunas entre os elementos.
    for i in a:
        file.write("{0}\n".format(i))

    file.close()

if __name__ == '__main__':
    for i in range(10,21):
        create_file(i)
