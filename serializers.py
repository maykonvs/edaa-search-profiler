import csv


class CSVSerializer:

    def serialize(self, results, filename):
        if len(results) == 0:
            return

        with open(filename, 'w', newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=vars(results[0]).keys())

            writer.writeheader()
            writer.writerows([vars(result) for result in results])
